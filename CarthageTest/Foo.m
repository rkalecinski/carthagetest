//
//  Foo.m
//  CarthageTest
//
//  Created by Robert Kaleciński on 14.02.2017.
//  Copyright © 2017 Fibaro. All rights reserved.
//

#import "Foo.h"

@implementation Foo

- (void)bar
{
    NSLog(@"Test succeed");
}

@end
