//
//  Foo.h
//  CarthageTest
//
//  Created by Robert Kaleciński on 14.02.2017.
//  Copyright © 2017 Fibaro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Foo : NSObject

- (void)bar;

@end
