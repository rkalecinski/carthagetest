//
//  CarthageTest.h
//  CarthageTest
//
//  Created by Robert Kaleciński on 14.02.2017.
//  Copyright © 2017 Fibaro. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CarthageTest.
FOUNDATION_EXPORT double CarthageTestVersionNumber;

//! Project version string for CarthageTest.
FOUNDATION_EXPORT const unsigned char CarthageTestVersionString[];

// In this header, you should import all the public headers of your framework using statements like

#import <CarthageTest/Foo.h>
